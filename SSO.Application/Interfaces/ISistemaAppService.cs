﻿using System;
using System.Collections.Generic;
using SSO.Application.ViewsModels;

namespace SSO.Application.Interfaces
{
    public interface ISistemaAppService : IDisposable
    {
        void Add(SistemaVm entity);

        void Update(SistemaVm entity);

        void Remove(int id);

        SistemaVm GetById(int id);

        IEnumerable<SistemaVm> GetAll();
    }
}