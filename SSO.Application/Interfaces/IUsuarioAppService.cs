﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SSO.Application.ViewsModels;

namespace SSO.Application.Interfaces
{
    public interface IUsuarioAppService : IDisposable
    {
        void Add(UsuarioVm entity);
        void Update(UsuarioVm entity);
        void Remove(Guid id);
        bool IsUniqueEmail(string email, Guid? id);
        bool IsUniqueUserName(string email, Guid? id);
        UsuarioVm GetById(Guid id);
        UsuarioVm Authenticate(LoginVm model);
        Task<UsuarioVm> AuthenticateAsync(string userName, string password);
        UsuarioVm GetByEmail(string email);
        IEnumerable<UsuarioVm> GetAll();
        void UpdateStatus(Guid id);
        void ResetPassword(Guid id);
    }
}