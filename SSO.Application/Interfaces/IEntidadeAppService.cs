﻿using System;
using System.Collections.Generic;
using SSO.Application.ViewsModels;

namespace SSO.Application.Interfaces
{
    public interface IEntidadeAppService : IDisposable
    {
        void Add(EntidadeVm entity);

        void Update(EntidadeVm entity);

        void Remove(int id);

        EntidadeVm GetById(int id);

        IEnumerable<EntidadeVm> GetAll(); 
    }
}