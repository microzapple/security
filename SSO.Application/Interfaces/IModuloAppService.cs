﻿using System;
using System.Collections.Generic;
using SSO.Application.ViewsModels;

namespace SSO.Application.Interfaces
{
    public interface IModuloAppService : IDisposable
    {
        void Add(ModuloVm entity);

        void Update(ModuloVm entity);

        void Remove(int id);

        ModuloVm GetById(int id);

        IEnumerable<ModuloVm> GetAll();
    }
}