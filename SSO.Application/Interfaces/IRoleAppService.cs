﻿using System;
using System.Collections.Generic;
using SSO.Application.ViewsModels;

namespace SSO.Application.Interfaces
{
    public interface IRoleAppService : IDisposable
    {
        void Add(RoleVm entity);
        void Update(RoleVm entity);
        bool IsUnique(string name, int? id);
        RoleVm GetById(int id);
        IEnumerable<RoleVm> GetAll();
        void Delete(int id);
    }
}