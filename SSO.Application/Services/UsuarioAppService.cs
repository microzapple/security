﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using SSO.Application.AutoMapper;
using SSO.Application.Interfaces;
using SSO.Application.ViewsModels;
using SSO.Domain.Entities;
using SSO.Domain.Interfaces.Service;

namespace SSO.Application.Services
{
    public class UsuarioAppService : IUsuarioAppService
    {
        private readonly IUsuarioService _userService;
        private readonly IMapper _mapper = AutoMapperConfig.MapperConfiguration.CreateMapper();

        public UsuarioAppService(IUsuarioService userService)
        {
            _userService = userService;
        }

        public void Add(UsuarioVm entity)
        {
            var user = new Usuario(entity.FirstName, entity.LastName, entity.UserName, entity.Email);
            _userService.Add(user);
        }

        public void Update(UsuarioVm entity)
        {
            _userService.Update(_mapper.Map<Usuario>(entity));
        }

        public void Remove(Guid id)
        {
            _userService.Remove(id);
        }

        public bool IsUniqueEmail(string email, Guid? id)
        {
            return _userService.IsUniqueEmail(email, id);
        }

        public bool IsUniqueUserName(string email, Guid? id)
        {
            return _userService.IsUniqueUserName(email, id);
        }

        public void UpdateStatus(Guid id)
        {
            _userService.UpdateStatus(id);
        }

        public UsuarioVm GetById(Guid id)
        {
            return _mapper.Map<UsuarioVm>(_userService.GetById(id));
        }

        public UsuarioVm Authenticate(LoginVm model)
        {
            return _mapper.Map<UsuarioVm>(_userService.Authenticate(model.UserName, model.Password));
        }

        public async Task<UsuarioVm> AuthenticateAsync(string userName, string password)
        {
            var user = await _userService.AuthenticateAsync(userName, password);
            return _mapper.Map<UsuarioVm>(user);
        }

        public UsuarioVm GetByEmail(string email)
        {
            return _mapper.Map<UsuarioVm>(_userService.GetByEmail(email));
        }

        public IEnumerable<UsuarioVm> GetAll()
        {
            return _mapper.Map<IEnumerable<UsuarioVm>>(_userService.GetAll());
        }

        public void ResetPassword(Guid id)
        {
            _userService.ResetPassword(id);
        }

        public void Dispose()
        {
            _userService.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}