﻿using System;
using System.Collections.Generic;
using AutoMapper;
using SSO.Application.AutoMapper;
using SSO.Application.Interfaces;
using SSO.Application.ViewsModels;
using SSO.Domain.Entities;
using SSO.Domain.Interfaces.Service;

namespace SSO.Application.Services
{
    public class ModuloAppService : IModuloAppService
    {
        private readonly IModuloService _moduleService;
        private readonly IMapper _mapper = AutoMapperConfig.MapperConfiguration.CreateMapper();

        public ModuloAppService(IModuloService moduleService)
        {
            _moduleService = moduleService;
        }

        public void Add(ModuloVm entity)
        {
            _moduleService.Add(_mapper.Map<Modulo>(entity));
        }

        public void Update(ModuloVm entity)
        {
            _moduleService.Update(_mapper.Map<Modulo>(entity));
        }

        public void Remove(int id)
        {
            _moduleService.Remove(id);
        }

        public ModuloVm GetById(int id)
        {
            return _mapper.Map<ModuloVm>(_moduleService.GetById(id));
        }

        public IEnumerable<ModuloVm> GetAll()
        {
            return _mapper.Map<IEnumerable<ModuloVm>>(_moduleService.GetAll());
        }

        public void Dispose()
        {
            _moduleService.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
