﻿using System;
using System.Collections.Generic;
using AutoMapper;
using SSO.Application.AutoMapper;
using SSO.Application.Interfaces;
using SSO.Application.ViewsModels;
using SSO.Domain.Interfaces.Service;

namespace SSO.Application.Services
{
    public class SistemaAppService : ISistemaAppService
    {
        private readonly ISistemaService _systemRepository;
        private readonly IMapper _mapper = AutoMapperConfig.MapperConfiguration.CreateMapper();

        public SistemaAppService(ISistemaService systemRepository)
        {
            _systemRepository = systemRepository;
        }

        public void Add(SistemaVm entity)
        {
            _systemRepository.Add(_mapper.Map<Domain.Entities.Sistema>(entity));
        }

        public void Update(SistemaVm entity)
        {
            _systemRepository.Update(_mapper.Map<Domain.Entities.Sistema>(entity));
        }

        public void Remove(int id)
        {
            _systemRepository.Remove(id);
        }

        public SistemaVm GetById(int id)
        {
            return _mapper.Map<SistemaVm>(_systemRepository.GetById(id));
        }

        public IEnumerable<SistemaVm> GetAll()
        {
            return _mapper.Map<IEnumerable<SistemaVm>>(_systemRepository.GetAll());
        }

        public void Dispose()
        {
            _systemRepository.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}