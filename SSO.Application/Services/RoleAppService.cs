﻿using System.Collections.Generic;
using AutoMapper;
using SSO.Application.AutoMapper;
using SSO.Application.Interfaces;
using SSO.Application.ViewsModels;
using SSO.Domain.Entities;
using SSO.Domain.Interfaces.Service;

namespace SSO.Application.Services
{
    public class RoleAppService : IRoleAppService
    {
        private readonly IRoleService _roleService;
        private readonly IMapper _mapper = AutoMapperConfig.MapperConfiguration.CreateMapper();

        public RoleAppService(IRoleService roleService)
        {
            _roleService = roleService;
        }

        public void Add(RoleVm entity)
        {
            _roleService.Add(_mapper.Map<Role>(entity));
        }

        public void Update(RoleVm entity)
        {
            _roleService.Update(_mapper.Map<Role>(entity));
        }

        public bool IsUnique(string name, int? id)
        {
            return _roleService.IsUnique(name, id);
        }

        public RoleVm GetById(int id)
        {
            return _mapper.Map<RoleVm>(_roleService.GetById(id));
        }

        public IEnumerable<RoleVm> GetAll()
        {
            return _mapper.Map<IEnumerable<RoleVm>>(_roleService.GetAll());
        }

        public void Delete(int id)
        {
            _roleService.Remove(id);
        }

        public void Dispose()
        {
            _roleService.Dispose();
        }
    }
}