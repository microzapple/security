﻿using System;
using System.Collections.Generic;
using AutoMapper;
using SSO.Application.AutoMapper;
using SSO.Application.Interfaces;
using SSO.Application.ViewsModels;
using SSO.Domain.Entities;
using SSO.Domain.Interfaces.Service;

namespace SSO.Application.Services
{
    public class EntidadeAppService : IEntidadeAppService
    {
        private readonly IEntidadeService _entidadeService;
        private readonly IMapper _mapper = AutoMapperConfig.MapperConfiguration.CreateMapper();

        public EntidadeAppService(IEntidadeService entidadeService)
        {
            _entidadeService = entidadeService;
        }

        public void Add(EntidadeVm entity)
        {
            _entidadeService.Add(_mapper.Map<Entidade>(entity));
        }

        public void Update(EntidadeVm entity)
        {
            _entidadeService.Update(_mapper.Map<Entidade>(entity));
        }

        public void Remove(int id)
        {
            _entidadeService.Remove(id);
        }

        public EntidadeVm GetById(int id)
        {
            return _mapper.Map<EntidadeVm>(_entidadeService.GetById(id));
        }

        public IEnumerable<EntidadeVm> GetAll()
        {
            return _mapper.Map<IEnumerable<EntidadeVm>>(_entidadeService.GetAll());
        }

        public void Dispose()
        {
            _entidadeService.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}