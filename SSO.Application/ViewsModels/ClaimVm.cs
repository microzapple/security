﻿using System;

namespace SSO.Application.ViewsModels
{
    public class ClaimVm
    {
        public int ClaimId { get; set; }
        public string Nome { get; set; }
        public string Tipo { get; set; }
        public virtual UsuarioVm Usuario { get; set; }
        public Guid UsuarioId { get; set; }
    }
}
