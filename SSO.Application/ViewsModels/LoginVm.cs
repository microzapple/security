﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SSO.Application.ViewsModels
{
    public class LoginVm
    {
        [Required]
        public string Identifier { get; set; }

        [Required]
        [DisplayName("Login")]
        [RegularExpression(@"[0-9]{1,20}", ErrorMessage = "Login só aceita números")]
        public string UserName { get; set; }

        [Required]
        [DisplayName("Senha")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}