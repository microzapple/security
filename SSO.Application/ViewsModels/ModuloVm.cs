﻿namespace SSO.Application.ViewsModels
{
    public class ModuloVm
    {
        public int ModuloId { get; set; }
        public string Nome { get; set; }

        public int? ModuloPaiId { get; set; }
        public virtual ModuloVm ModuloPai { get; set; }
    }
}