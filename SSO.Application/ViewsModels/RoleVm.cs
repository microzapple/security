﻿using System.Collections.Generic;

namespace SSO.Application.ViewsModels
{
    public class RoleVm
    {
        public int RoleId { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }

        public virtual ICollection<SistemaVm> ListaSistemas { get; set; }
    }
}
