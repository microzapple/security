﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SSO.Helper.Resources;

namespace SSO.Application.ViewsModels
{
    public class SistemaVm
    {
        public int SistemaId { get; set; }

        [DisplayName("Nome")]
        [Required(ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Required")]
        [MaxLength(14, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "MaxLength")]
        public string Nome { get; set; }

        [DisplayName("Descrição")]
        [Required(ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Required")]
        [MinLength(10, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "MinLength")]
        [MaxLength(50, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "MaxLength")]
        public string Descricao { get; set; }

        public virtual ICollection<UsuarioVm> ListaUsuarios { get; set; }

        public virtual ICollection<RoleVm> ListaRoles { get; set; }

        public void Save()
        {


        }
    }
}