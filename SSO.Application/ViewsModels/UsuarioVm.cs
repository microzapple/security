﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SSO.Helper.Resources;

namespace SSO.Application.ViewsModels
{
    public class UsuarioVm
    {
        public Guid UsuarioId { get; set; }

        [DisplayName("Nome")]
        [Required(ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Required")]
        [MaxLength(30, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "MaxLength")]
        public string FirstName { get; set; }

        [DisplayName("Sobrenome")]
        [Required(ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Required")]
        [MaxLength(30, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "MaxLength")]
        public string LastName { get; set; }

        [DisplayName("Login")]
        [Required(ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Required")]
        [MinLength(8, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "MinLength")]
        [MaxLength(14, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "MaxLength")]
        public string UserName { get; set; }

        [EmailAddress]
        [Required(ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "Required")]
        [MaxLength(60, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "MaxLength")]
        public string Email { get; set; }

        [DisplayName("Senha")]
        [MinLength(6, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "MinLength")]
        [MaxLength(12, ErrorMessageResourceType = typeof(Errors), ErrorMessageResourceName = "MaxLength")]
        public string Password { get; set; }

        [DisplayName("Ativo")]
        public bool Active { get; set; }

        public virtual ICollection<SistemaVm> ListaSistemas { get; set; }

        public virtual ICollection<ClaimVm> ListaClaims { get; set; }
        public virtual ICollection<RoleVm> ListaRoles { get; set; }

        public virtual ICollection<EntidadeVm> ListaEntidades { get; set; }

        public string FullName { get { return string.Format("{0} {1}", FirstName, LastName); } }
    }
}