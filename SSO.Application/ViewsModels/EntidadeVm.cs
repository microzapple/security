﻿using System.Collections.Generic;

namespace SSO.Application.ViewsModels
{
    public class EntidadeVm
    {
        public int EntidadeId { get; set; }
        public string Nome { get; set; }
        public virtual ICollection<UsuarioVm> ListaUsuarios { get; set; }
    }
}
