﻿using AutoMapper;
using SSO.Application.ViewsModels;
using SSO.Domain.Entities;

namespace SSO.Application.AutoMapper
{
    public static class AutoMapperConfig
    {
        public static MapperConfiguration MapperConfiguration;
        
        public static void RegisterMappings()
        {
            MapperConfiguration = new MapperConfiguration(x =>
            {
                x.CreateMap<Modulo, ModuloVm>().MaxDepth(2).ReverseMap();
                x.CreateMap<Sistema, SistemaVm>().MaxDepth(2).ReverseMap();
                x.CreateMap<Usuario, UsuarioVm>().MaxDepth(2).ReverseMap();
                x.CreateMap<Entidade, EntidadeVm>().MaxDepth(2).ReverseMap();
                x.CreateMap<Claim, ClaimVm>().MaxDepth(2).ReverseMap();
                x.CreateMap<Role, RoleVm>().MaxDepth(2).ReverseMap();
            });
        }
    }
}