﻿using System;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using SSO.Services.Api.Providers;

namespace SSO.Services.Api
{
	 public partial class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static string PublicClientId { get; private set; }

        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the application for OAuth based flow
            PublicClientId = "self";
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                Provider = new ApplicationOAuthProvider(PublicClientId),
                AuthorizeEndpointPath = new PathString("/v1/Account/Get"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
                //TODO In production mode set 
                //AllowInsecureHttp = false
                AllowInsecureHttp = true
            };
        }
    }
}