﻿using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

namespace SSO.Services.Api.Controllers
{
    [RoutePrefix("v1/account")]
    public class AccountController : ApiController
    {
        HttpResponseMessage _response;

        [HttpGet]
        [AllowAnonymous]
        [Route("get")]
        public IHttpActionResult Get()
        {
            return Ok("AGORA FOI");
            var caller = User as ClaimsPrincipal;

            var subjectClaim = caller.FindFirst("sub");
            if (subjectClaim != null)
            {
                return Json(new
                {
                    message = "OK user",
                    client = caller.FindFirst("client_id").Value,
                    subject = subjectClaim.Value
                });
            }
            return Json(new { message = "OK computer", client = caller.FindFirst("client_id").Value });
        }

        [Route("test")]
        [HttpGet]
        [HostAuthentication("ExternalBearer")]
        public Task<HttpResponseMessage> Test()
        {
            _response = Request.CreateResponse(HttpStatusCode.OK, "TESTADO");
            return GetResult(_response);
        }

        private static Task<HttpResponseMessage> GetResult(HttpResponseMessage response)
        {
            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response);
            return tsc.Task;
        }
    }
}