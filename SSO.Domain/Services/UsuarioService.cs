﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SSO.Domain.Entities;
using SSO.Domain.Interfaces.Repository;
using SSO.Domain.Interfaces.Service;
using SSO.Domain.Validation;
using SSO.Helper.Resources;

namespace SSO.Domain.Services
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUsuarioRepository _userRepository;

        public UsuarioService(IUsuarioRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public void Add(Usuario entity)
        {
            if (IsUniqueEmail(entity.Email, null)) throw new Exception(Errors.DuplicateEmail);
            if (IsUniqueUserName(entity.UserName, null)) throw new Exception(Errors.DuplicateUserName);

            var password = entity.UserName;
            entity.SetPassword(password, password);
            entity.Validate();
            _userRepository.Add(entity);
        }

        public void Update(Usuario entity)
        {
            var oldUser = _userRepository.GetById(entity.UsuarioId);

            if (IsUniqueEmail(entity.Email, entity.UsuarioId)) throw new Exception(Errors.DuplicateEmail);
            if (IsUniqueUserName(entity.UserName, entity.UsuarioId)) throw new Exception(Errors.DuplicateUserName);

            oldUser.UpdateInformation(entity);
            _userRepository.Update(oldUser);
        }

        public void Remove(Guid id)
        {
            _userRepository.Remove(id);
        }

        public bool IsUniqueEmail(string email, Guid? id)
        {
            return _userRepository.IsUniqueEmail(email, id);
        }

        public bool IsUniqueUserName(string userName, Guid? id)
        {
            return _userRepository.IsUniqueUserName(userName, id);
        }

        public Usuario GetById(Guid id)
        {
            return _userRepository.GetById(id);
        }

        public Usuario Authenticate(string userName, string password)
        {
            var user = _userRepository.Authenticate(userName, PasswordAssertionConcern.Encrypt(password));
            if (user == null) throw new Exception(Errors.InvalidCredentials);
            return user;
        }

        public async Task<Usuario> AuthenticateAsync(string userName, string password)
        {
            var user = await _userRepository.AuthenticateAsync(userName, PasswordAssertionConcern.Encrypt(password));
            if (user == null) throw new Exception(Errors.InvalidCredentials);
            return user;
        }

        public Usuario GetByEmail(string email)
        {
            return _userRepository.GetByEmail(email);
        }

        public IEnumerable<Usuario> GetAll()
        {
            return _userRepository.GetAll();
        }

        public void UpdateStatus(Guid id)
        {
            var user = _userRepository.GetById(id);
            user.Active = !user.Active;
            _userRepository.Update(user);
        }

        public void ResetPassword(Guid id)
        {
            var user = _userRepository.GetById(id);
            user.ResetPassword();
            _userRepository.Update(user);
        }

        public void Dispose()
        {
            _userRepository.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}