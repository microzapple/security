﻿using System;
using System.Collections.Generic;
using SSO.Domain.Entities;
using SSO.Domain.Interfaces.Repository;
using SSO.Domain.Interfaces.Service;

namespace SSO.Domain.Services
{
    public class SistemaService : ISistemaService
    {
        private readonly ISistemaRepository _systemRepository;

        public SistemaService(ISistemaRepository systemRepository)
        {
            _systemRepository = systemRepository;
        }

        public void Add(Sistema entity)
        {
            var nomeExist = _systemRepository.IsUniqueNome(entity.Nome, null);
            if (nomeExist) throw new Exception("Este sistema já existe");
            _systemRepository.Add(entity);
        }

        public void Update(Sistema entity)
        {
            _systemRepository.Update(entity);
        }

        public void Remove(int id)
        {
            _systemRepository.Remove(id);
        }

        public bool IsUniqueNome(string nome, int? id)
        {
            return _systemRepository.IsUniqueNome(nome, id);
        }

        public Sistema GetById(int id)
        {
            return _systemRepository.GetById(id);
        }

        public IEnumerable<Sistema> GetAll()
        {
            return _systemRepository.GetAll();
        }

        public void Dispose()
        {
            _systemRepository.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}