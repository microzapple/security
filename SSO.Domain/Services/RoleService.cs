﻿using System.Collections.Generic;
using SSO.Domain.Entities;
using SSO.Domain.Interfaces.Repository;
using SSO.Domain.Interfaces.Service;

namespace SSO.Domain.Services
{
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository _roleRepository;

        public RoleService(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        public void Add(Role entity)
        {
            _roleRepository.Add(entity);
        }

        public void Update(Role entity)
        {
            _roleRepository.Update(entity);
        }

        public void Remove(int id)
        {
            _roleRepository.Remove(id);
        }

        public bool IsUnique(string name, int? id)
        {
            return _roleRepository.IsUnique(name, id);
        }

        public Role GetById(int id)
        {
            return _roleRepository.GetById(id);
        }

        public IEnumerable<Role> GetAll()
        {
            return _roleRepository.GetAll();
        }

        public void Dispose()
        {
            _roleRepository.Dispose();
        }
    }
}