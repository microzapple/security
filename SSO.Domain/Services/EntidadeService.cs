﻿using System;
using SSO.Domain.Interfaces.Repository;
using SSO.Domain.Interfaces.Service;

namespace SSO.Domain.Services
{
    public class EntidadeService : IEntidadeService
    {
        private readonly IEntidadeRepository _entidadeRepository;

        public EntidadeService(IEntidadeRepository entidadeRepository)
        {
            _entidadeRepository = entidadeRepository;
        }

        public void Add(Entities.Entidade entity)
        {
            _entidadeRepository.Add(entity);
        }

        public void Update(Entities.Entidade entity)
        {
            _entidadeRepository.Update(entity);
        }

        public void Remove(int id)
        {
            _entidadeRepository.Remove(id);
        }

        public Entities.Entidade GetById(int id)
        {
            return _entidadeRepository.GetById(id);
        }

        public System.Collections.Generic.IEnumerable<Entities.Entidade> GetAll()
        {
            return _entidadeRepository.GetAll();
        }

        public void Dispose()
        {
            _entidadeRepository.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}