﻿using System;
using System.Collections.Generic;
using SSO.Domain.Entities;
using SSO.Domain.Interfaces.Repository;
using SSO.Domain.Interfaces.Service;

namespace SSO.Domain.Services
{
    public class ModuloService : IModuloService
    {
        private readonly IModuloRepository _moduleRepository;

        public ModuloService(IModuloRepository moduleRepository)
        {
            _moduleRepository = moduleRepository;
        }

        public void Add(Modulo entity)
        {
            _moduleRepository.Add(entity);
        }

        public void Update(Modulo entity)
        {
            _moduleRepository.Update(entity);
        }

        public void Remove(int id)
        {
            _moduleRepository.Remove(id);
        }

        public Modulo GetById(int id)
        {
            return _moduleRepository.GetById(id);
        }

        public IEnumerable<Modulo> GetAll()
        {
            return _moduleRepository.GetAll();
        }

        public void Dispose()
        {
            _moduleRepository.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
