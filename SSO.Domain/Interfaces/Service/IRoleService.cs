﻿using System;
using System.Collections.Generic;
using SSO.Domain.Entities;

namespace SSO.Domain.Interfaces.Service
{
    public interface IRoleService : IDisposable
    {
        void Add(Role entity);
        void Update(Role entity);
        void Remove(int id);
        bool IsUnique(string name, int? id);
        Role GetById(int id);
        IEnumerable<Role> GetAll();
    }
}