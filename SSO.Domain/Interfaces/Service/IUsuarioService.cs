﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SSO.Domain.Entities;

namespace SSO.Domain.Interfaces.Service
{
    public interface IUsuarioService : IDisposable
    {
        void Add(Usuario entity);
        void Update(Usuario entity);
        void Remove(Guid id);
        bool IsUniqueEmail(string email, Guid? id);
        bool IsUniqueUserName(string email, Guid? id);
        Usuario GetById(Guid id);
        Usuario Authenticate(string userName, string password);
        Task<Usuario> AuthenticateAsync(string userName, string password);
        Usuario GetByEmail(string email);
        IEnumerable<Usuario> GetAll(); 
        void UpdateStatus(Guid id);
        void ResetPassword(Guid id);
    }
}