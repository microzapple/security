﻿using System;
using System.Collections.Generic;
using SSO.Domain.Entities;

namespace SSO.Domain.Interfaces.Service
{
    public interface IModuloService : IDisposable
    {
        void Add(Modulo entity);

        void Update(Modulo entity);

        void Remove(int id);

        Modulo GetById(int id);

        IEnumerable<Modulo> GetAll();
    }
}