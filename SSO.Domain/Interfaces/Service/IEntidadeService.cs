﻿using System;
using System.Collections.Generic;
using SSO.Domain.Entities;

namespace SSO.Domain.Interfaces.Service
{
    public interface IEntidadeService : IDisposable
    {
        void Add(Entidade entity);

        void Update(Entidade entity);

        void Remove(int id);

        Entidade GetById(int id);

        IEnumerable<Entidade> GetAll();
    }
}