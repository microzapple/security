﻿using System;
using System.Collections.Generic;
using SSO.Domain.Entities;

namespace SSO.Domain.Interfaces.Service
{
    public interface ISistemaService : IDisposable
    {
        void Add(Sistema entity);
        void Update(Sistema entity);
        void Remove(int id);
        bool IsUniqueNome(string email, int? id);
        Sistema GetById(int id);
        IEnumerable<Sistema> GetAll();
    }
}