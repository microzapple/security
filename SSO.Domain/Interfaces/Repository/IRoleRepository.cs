﻿using SSO.Domain.Entities;

namespace SSO.Domain.Interfaces.Repository
{
    public interface IRoleRepository : IRepository<Role>
    {
        bool IsUnique(string name, int? id);
    }
}