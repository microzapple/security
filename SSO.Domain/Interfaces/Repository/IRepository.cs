﻿using System;
using System.Collections.Generic;

namespace SSO.Domain.Interfaces.Repository
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        void Add(TEntity entity);

        void Update(TEntity entity);

        void Remove(int id);

        void Remove(Guid id);

        TEntity GetById(int id);

        TEntity GetById(Guid id);

        IEnumerable<TEntity> GetAll();

        int SaveChanges();
    }
}