﻿using SSO.Domain.Entities;

namespace SSO.Domain.Interfaces.Repository
{
    public interface IEntidadeRepository : IRepository<Entidade>
    {
    }
}
