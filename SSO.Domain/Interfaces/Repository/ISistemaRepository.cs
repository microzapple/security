﻿using SSO.Domain.Entities;

namespace SSO.Domain.Interfaces.Repository
{
    public interface ISistemaRepository : IRepository<Sistema>
    {
        bool IsUniqueNome(string nome, int? id);
    }
}