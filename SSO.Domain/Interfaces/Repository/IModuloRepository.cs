﻿using SSO.Domain.Entities;

namespace SSO.Domain.Interfaces.Repository
{
    public interface IModuloRepository : IRepository<Modulo>
    {
         
    }
}