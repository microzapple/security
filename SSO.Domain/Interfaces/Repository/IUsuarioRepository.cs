﻿using System;
using System.Threading.Tasks;
using SSO.Domain.Entities;

namespace SSO.Domain.Interfaces.Repository
{
    public interface IUsuarioRepository : IRepository<Usuario>
    {
        Usuario Authenticate(string userName, string password);
        Task<Usuario> AuthenticateAsync(string userName, string password);
        Usuario GetByEmail(string email);
        Usuario GetByUserName(string userName);
        bool IsUniqueEmail(string email, Guid? id);
        bool IsUniqueUserName(string userName, Guid? id);
    }
}