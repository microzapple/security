﻿using System;
using System.Text.RegularExpressions;
using SSO.Helper.Resources;

namespace SSO.Domain.Validation
{
    public class EmailAssertionConcern
    {
        public static void AssertIsValid(string email)
        {
            AssertionConcern.AssertArgumentNotNull(email, Errors.Required);
            AssertEmailIsValid(email);
            var domain = "@gov.br";
            //AssertDomainIsValid(email, domain);
        }

        public static void AssertEmailIsValid(string email)
        {
            if (!Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
                throw new InvalidOperationException(Errors.InvalidEmail);
        }

        public static void AssertDomainIsValid(string email, string domain)
        {
            var posicao = email.IndexOf("@", StringComparison.Ordinal);
            if (email.Length - posicao < domain.Length) 
                throw new InvalidOperationException(Errors.InvalidDomain);
            if (email.Substring(posicao, domain.Length) != domain)
                throw new InvalidOperationException(Errors.InvalidDomain);
        }
    }
}