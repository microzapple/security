﻿namespace SSO.Domain.Entities
{
    public class Modulo
    {
        public int ModuloId { get; set; }
        public string Nome { get; set; }
        public int? ModuloPaiId { get; set; }
        public virtual Modulo ModuloPai { get; set; }
    }
}