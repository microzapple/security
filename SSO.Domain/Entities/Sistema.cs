﻿using System.Collections.Generic;

namespace SSO.Domain.Entities
{
    public class Sistema
    {
        public int SistemaId { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public virtual ICollection<Role> ListaRoles { get; set; }
        public virtual ICollection<Usuario> ListaUsuarios { get; set; }
    }
}