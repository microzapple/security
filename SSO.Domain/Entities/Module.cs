﻿namespace SSO.Domain.Entities
{
    public class Module
    {
        public int ModuleId { get; set; }
        public string Name { get; set; }

        public int? ModuleFatherId { get; set; }
        public virtual Module ModuleFather { get; set; }
    }
}