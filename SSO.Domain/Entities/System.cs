﻿using System.Collections.Generic;

namespace SSO.Domain.Entities
{
    public class System
    {
        public int SystemId { get; set; }
        
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual List<User>  ListUser { get; set; }

        public void Save()
        {
            

        }
    }
}