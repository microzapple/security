﻿using System.Collections.Generic;

namespace SSO.Domain.Entities
{
    public class Entidade
    {
        public int EntidadeId { get; set; }
        public string Nome { get; set; }
        public virtual ICollection<Usuario> ListaUsuarios { get; set; }
    }
}
