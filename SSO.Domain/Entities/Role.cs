﻿using System.Collections.Generic;

namespace SSO.Domain.Entities
{
    public class Role
    {
        public int RoleId { get; set; }
        public string Nome { get; set; }
        public virtual ICollection<Sistema> ListaSistemas { get; set; }
    }
}