﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace SSO.Domain.Entities
{
    public class User
    {
        public Guid UserId { get; private set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }

        public string Email { get; set; }

        [DisplayName("Senha")]
        public string Password { get; set; }

        [DisplayName("Ativo")]
        public bool Active { get; set; }

        public virtual List<System> ListSystem { get; set; }
        public string FullName { get { return string.Format("{0} {1}", FirstName, LastName); } }

        public User() { UserId = Guid.NewGuid(); }

        //public Usersss(string firstName, string lastName, string userName, string email)
        //{
        //    FirstName = firstName;
        //    LastName = lastName;
        //    UserName = userName;
        //    Email = email;

        //}

        public void SetPassword(string password, string confirmPassword)
        {
            //AssertionConcern.AssertArgumentNotNull(password, Errors.Required);
            //AssertionConcern.AssertArgumentNotNull(confirmPassword, Errors.RequiredConfirmPassword);
            //AssertionConcern.AssertArgumentLength(password, 6, 15, Errors.InvalidLimitPassword);
            //AssertionConcern.AssertArgumentEquals(password, confirmPassword, Errors.PasswordDoesMatch);

            //Password = PasswordAssertionConcern.Encrypt(password);
        }

        public string ResetPassword()
        {
            ////var password = Guid.NewGuid().ToString().Substring(0, 8);
            //var position = Email.IndexOf("@", StringComparison.Ordinal);
            //var password = Email.Substring(position, Email.Length - position);
            //Password = PasswordAssertionConcern.Encrypt(password);

            return string.Empty;
            //return password;
        }

        public void ChangeEmail(string email)
        {
            Email = email;
        }

        public void Validate()
        {
            //PasswordAssertionConcern.AssertIsValid(Password);
            //EmailAssertionConcern.AssertIsValid(Email);
        }

        public void Save()
        {
            var password = DateTime.Now.ToString("Y").Replace(" ", "");
            SetPassword(password, password);
            Validate();
            Active = true;
        }
    }
}