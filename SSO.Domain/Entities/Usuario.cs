﻿using System;
using System.Collections.Generic;
using SSO.Domain.Validation;

namespace SSO.Domain.Entities
{
    public class Usuario
    {
        public Guid UsuarioId { get; private set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool Active { get; set; }

        public virtual ICollection<Sistema> ListaSistemas { get; set; }

        public virtual ICollection<Entidade> ListaEntidades { get; set; }

        public virtual ICollection<Claim> ListaClaims { get; set; }

        private Usuario() { }

        public Usuario(string firstName, string lastName, string userName, string email)
        {
            FirstName = firstName;
            LastName = lastName;
            UserName = userName;
            Email = email;
            UsuarioId = Guid.NewGuid();
            Active = true;
        }

        public void SetPassword(string password, string confirmPassword)
        {
            AssertionConcern.AssertArgumentEquals(password, confirmPassword, "Senha e Confirmação não confere");
            Password = PasswordAssertionConcern.Encrypt(password);
        }

        public string ResetPassword()
        {
            var password = UserName;
            Password = PasswordAssertionConcern.Encrypt(password);
            return password;
        }

        public void ChangeEmail(string email)
        {
            Email = email;
        }

        public void Validate()
        {
            PasswordAssertionConcern.AssertIsValid(Password);
            EmailAssertionConcern.AssertIsValid(Email);
        }

        public void UpdateInformation(Usuario user)
        {
            UserName = user.UserName;
            Email = user.Email;
            FirstName = user.FirstName;
            LastName = user.LastName;
            Active = user.Active;
        }
    }
}