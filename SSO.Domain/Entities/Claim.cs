﻿using System;

namespace SSO.Domain.Entities
{
    public class Claim
    {
        public int ClaimId { get; set; }
        public string Nome { get; set; }
        public string Tipo { get; set; }
        public virtual Usuario Usuario { get; set; }
        public Guid UsuarioId { get; set; }
    }
}