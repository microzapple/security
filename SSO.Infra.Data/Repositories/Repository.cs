﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SSO.Domain.Interfaces.Repository;
using SSO.Infra.Data.Contexts;

namespace SSO.Infra.Data.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class 
    {
        protected ContextSso Db;
        protected DbSet<TEntity> DbSet;

        public Repository()
        {
            Db = new ContextSso();
            DbSet = Db.Set<TEntity>();
        }

        public virtual void Add(TEntity entity)
        {
            DbSet.Add(entity);
            SaveChanges();
        }

        public virtual void Update(TEntity entity)
        {
            var entry = Db.Entry(entity);
            DbSet.Attach(entity);
            entry.State = EntityState.Modified;
            SaveChanges();
        }

        public virtual void Remove(Guid id)
        {
            DbSet.Remove(GetById(id));
            SaveChanges();
        }

        public virtual void Remove(int id)
        {
            DbSet.Remove(GetById(id));
            SaveChanges();
        }

        public virtual TEntity GetById(Guid id)
        {
            return DbSet.Find(id);
        }

        public virtual TEntity GetById(int id)
        {
            return DbSet.Find(id);
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return DbSet.ToList();
        }

        public virtual int SaveChanges()
        {
            return Db.SaveChanges();
        }

        public void Dispose()
        {
            Db.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}