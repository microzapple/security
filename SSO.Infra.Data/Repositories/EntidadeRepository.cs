﻿using SSO.Domain.Entities;
using SSO.Domain.Interfaces.Repository;

namespace SSO.Infra.Data.Repositories
{
    public class EntidadeRepository : Repository<Entidade>, IEntidadeRepository
    {
    }
}
