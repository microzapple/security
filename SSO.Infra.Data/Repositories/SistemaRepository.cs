﻿using System.Linq;
using SSO.Domain.Entities;
using SSO.Domain.Interfaces.Repository;

namespace SSO.Infra.Data.Repositories
{
    public class SistemaRepository : Repository<Sistema>, ISistemaRepository
    {
        public bool IsUniqueNome(string nome, int? id)
        {
            return DbSet.Any(a => a.Nome.Equals(nome) && (!id.HasValue || id.Value != a.SistemaId));
        }
    }
}