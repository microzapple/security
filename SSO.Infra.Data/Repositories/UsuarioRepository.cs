﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using SSO.Domain.Entities;
using SSO.Domain.Interfaces.Repository;

namespace SSO.Infra.Data.Repositories
{
    public class UsuarioRepository : Repository<Usuario>, IUsuarioRepository
    {
        public Usuario Authenticate(string userName, string password)
        {
            return DbSet.Include(x => x.ListaClaims).Include(x => x.ListaEntidades).FirstOrDefault(x => x.UserName.Equals(userName) && x.Password.Equals(password));
        }

        public async Task<Usuario> AuthenticateAsync(string userName, string password)
        {
            return await DbSet.Include(x => x.ListaClaims).Include(x => x.ListaEntidades).FirstOrDefaultAsync(x => x.UserName.Equals(userName) && x.Password.Equals(password));
        }

        public Usuario GetByEmail(string email)
        {
            return DbSet.FirstOrDefault(x => x.Email.Equals(email));
        }

        public Usuario GetByUserName(string userName)
        {
            return DbSet.FirstOrDefault(f => f.UserName.Equals(userName));
        }

        public bool IsUniqueEmail(string email, Guid? id)
        {
            return DbSet.Any(a => a.Email.Equals(email) && (!id.HasValue || id.Value != a.UsuarioId));
        }

        public bool IsUniqueUserName(string userName, Guid? id)
        {
            return DbSet.Any(a => a.UserName.Equals(userName) && (!id.HasValue || id.Value != a.UsuarioId));
        }
    }
}