﻿using System.Linq;
using SSO.Domain.Entities;
using SSO.Domain.Interfaces.Repository;

namespace SSO.Infra.Data.Repositories
{
    public class RoleRepository : Repository<Role>, IRoleRepository
    {
        public bool IsUnique(string name, int? id)
        {
            return DbSet.Any(a => (a.Nome.ToLower().Equals(name.ToLower()) && id.HasValue && a.RoleId == id) || (id == null && !a.Nome.ToLower().Equals(name.ToLower())));
        }
    }
}