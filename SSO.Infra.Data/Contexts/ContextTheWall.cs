﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using SSO.Domain.Entities;
using SSO.Infra.Data.EntityConfig;

namespace SSO.Infra.Data.Contexts
{
    public class ContextSso : DbContext
    {
        public ContextSso() : base("Connection_SSO") { }

        public virtual DbSet<Usuario> Usuarios { get; set; }
        public virtual DbSet<Sistema> Sistemas { get; set; }
        public virtual DbSet<Modulo> Modulos { get; set; }
        public virtual DbSet<Entidade> Entidades { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Properties().Where(p => p.Name == p.ReflectedType.Name + "Id").Configure(p => p.IsKey());
            modelBuilder.Properties<string>().Configure(p => p.HasColumnType("varchar"));
            modelBuilder.Properties<string>().Configure(p => p.HasMaxLength(50));

            modelBuilder.Configurations.Add(new UsuarioConfig());

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            try
            {
                //foreach (var entry in ChangeTracker.Entries().Where(x => x.Entity.GetType().GetProperty("DataCadastro") != null))
                //{
                //    if (entry.State == EntityState.Added)
                //    {
                //        entry.Property("DataCadastro").CurrentValue = DateTime.Now;
                //    }
                //    if (entry.State == EntityState.Modified)
                //    {
                //        entry.Property("DataCadastro").IsModified = false;
                //    }
                //}
                return base.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErro in dbEx.EntityValidationErrors.SelectMany(validationErros => validationErros.ValidationErrors))
                {
                    Trace.TraceInformation("Propriedade: {0}, Erro: {1}", validationErro.PropertyName, validationErro.ErrorMessage);
                }
                return 0;
            }
        }
    }
}