﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using SSO.Domain.Entities;

namespace SSO.Infra.Data.EntityConfig
{
    public class UsuarioConfig : EntityTypeConfiguration<Usuario>
    {
        public UsuarioConfig()
        {
            HasKey(x => x.UsuarioId);
            Property(x => x.Email).IsRequired().HasMaxLength(60);
            Property(x => x.FirstName).IsRequired().HasMaxLength(30);
            Property(x => x.LastName).IsRequired().HasMaxLength(30);
            Property(x => x.UserName).IsRequired().HasMaxLength(14).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_USERNAME")));
            Property(x => x.Password).IsRequired().HasMaxLength(128);
            Property(x => x.Active).IsRequired();

            HasMany(x => x.ListaSistemas).WithMany(s => s.ListaUsuarios).Map(m => m.MapLeftKey("UsuarioId").MapRightKey("SistemaId"));

            HasMany(x => x.ListaClaims).WithRequired(s => s.Usuario).HasForeignKey(x => x.UsuarioId);
        }
    }
}