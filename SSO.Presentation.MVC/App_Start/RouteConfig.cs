﻿using System.Web.Mvc;
using System.Web.Routing;

namespace SSO.Presentation.MVC
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("Default", "{controller}/{action}/{id}", new {controller = "Home", action = "Index", id = UrlParameter.Optional});
            routes.MapRoute("Security", "identity/account/login/{id}", new {controller = "Account", action = "Login", id = UrlParameter.Optional});
        }
    }
}