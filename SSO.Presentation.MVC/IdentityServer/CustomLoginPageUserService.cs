﻿using IdentityServer3.Core.Services.Default;
using System.Collections.Generic;
using System.Threading.Tasks;
using IdentityServer3.Core.Models;
using System.Security.Claims;
using IdentityServer3.Core.Services;
using Microsoft.Owin;

namespace SSO.Presentation.MVC.IdentityServer
{
    public class CustomLoginPageUserService : UserServiceBase
    {
        private readonly OwinContext _ctx;

        public CustomLoginPageUserService(OwinEnvironmentService owinEnv)
        {
            _ctx = new OwinContext(owinEnv.Environment);
        }

        public override Task PreAuthenticateAsync(PreAuthenticationContext context)
        {
            var id = _ctx.Request.Query.Get("signin");
            context.AuthenticateResult = new AuthenticateResult(string.Format("~/account/login/{0}", id), (IEnumerable<Claim>)null);
            return Task.FromResult(0);
        }
    }
}