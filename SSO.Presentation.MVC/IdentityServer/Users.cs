﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using IdentityServer3.Core;
using IdentityServer3.Core.Services.InMemory;
using SSO.Application.ViewsModels;

namespace SSO.Presentation.MVC.IdentityServer
{
    public static class Users
    {
        public static InMemoryUser GetInMemoryUser(UsuarioVm user)
        {
            var inMemoryUsers = new InMemoryUser
            {
                Username = user.UserName,
                Password = user.Password,
                Subject = user.UsuarioId.ToString(),
                Claims = GetClaims(user)
            };
            return inMemoryUsers;
        }

        public static List<Claim> GetClaims(UsuarioVm user)
        {
            var claims = new List<Claim>
            {
                  new Claim(Constants.ClaimTypes.Name, user.FullName),
                  new Claim(Constants.ClaimTypes.GivenName, user.FirstName),   
                  new Claim(Constants.ClaimTypes.FamilyName, user.LastName),
                  new Claim(Constants.ClaimTypes.Subject, user.UsuarioId.ToString()),
                  new Claim(Constants.ClaimTypes.Email, user.Email),
                  new Claim(Constants.ClaimTypes.Id, user.Email),
                  new Claim(Constants.ClaimTypes.NickName, user.UserName)
            };
            if (user.ListaRoles != null && user.ListaRoles.Any()) claims.AddRange(user.ListaRoles.Select(s => new Claim(Constants.ClaimTypes.Role, s.Nome)));
            return claims;
        }
    }
}