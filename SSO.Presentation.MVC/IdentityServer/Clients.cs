﻿using System.Collections.Generic;
using IdentityServer3.Core.Models;

namespace SSO.Presentation.MVC.IdentityServer
{
    public static class Clients
    {
        public static IEnumerable<Client> Get()
        {
            return new[]
            {
                new Client
                {
                    Enabled = true,
                    ClientName = "MVC Client",
                    ClientId = "mvc",
                    Flow = Flows.Implicit,
                    RedirectUris = new List<string>
                    {
                        "https://localhost:44305/"
                    },
                    PostLogoutRedirectUris = new List<string> 
                    {
                        "https://localhost:44305/"   
                    },
                    RequireConsent = false, //Não perguntar ao usuário se permite acesso a roles...
                    AllowAccessToAllScopes = true
                }
            };
        }
    }
}