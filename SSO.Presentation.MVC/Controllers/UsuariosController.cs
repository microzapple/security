﻿using System;
using System.Data.Entity;
using System.Net;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using SSO.Application.Interfaces;
using SSO.Application.ViewsModels;

namespace SSO.Presentation.MVC.Controllers
{
    [Authorize]
    public class UsuariosController : Controller
    {
        private readonly IUsuarioAppService _usuarioApp;

        public UsuariosController(IUsuarioAppService usuarioApp)
        {
            _usuarioApp = usuarioApp;
        }

        public ActionResult Index()
        {
            return View(_usuarioApp.GetAll());
        }

        public ActionResult Details(Guid? id)
        {
            if (id == null)return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            
            var usuario = _usuarioApp.GetById(id.Value);
            if (usuario == null)return HttpNotFound();
            return View(usuario);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UsuarioVm usuario)
        {
            if (!ModelState.IsValid) return View(usuario);
            _usuarioApp.Add(usuario);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var usuario = _usuarioApp.GetById(id.Value);
            if (usuario == null) return HttpNotFound();
            
            return View(usuario);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UsuarioVm usuario)
        {
            if (!ModelState.IsValid) return View(usuario);
            _usuarioApp.Update(usuario);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(Guid? id)
        {
            if (id == null)return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            
            var usuario = _usuarioApp.GetById(id.Value);
            if (usuario == null)return HttpNotFound();
            return View(usuario);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            _usuarioApp.Remove(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _usuarioApp.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
