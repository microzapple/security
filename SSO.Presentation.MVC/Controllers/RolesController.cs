﻿using System.Net;
using System.Web.Mvc;
using SSO.Application.Interfaces;
using SSO.Application.ViewsModels;

namespace SSO.Presentation.MVC.Controllers
{
    [Authorize]
    public class RolesController : Controller
    {
        private readonly IRoleAppService _roleApp;

        public RolesController(IRoleAppService roleApp)
        {
            _roleApp = roleApp;
        }

        public ActionResult Index()
        {
            return View(_roleApp.GetAll());
        }

        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var role = _roleApp.GetById(id.Value);
            if (role == null) return HttpNotFound();
            return View(role);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RoleVm role)
        {
            if (!ModelState.IsValid) return View(role);
            _roleApp.Add(role);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var role = _roleApp.GetById(id.Value);
            if (role == null) return HttpNotFound();
            return View(role);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RoleVm role)
        {
            if (ModelState.IsValid)
            {
                _roleApp.Update(role);
                return RedirectToAction("Index");
            }
            return View(role);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var role = _roleApp.GetById(id.Value);
            if (role == null) return HttpNotFound();
            return View(role);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _roleApp.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)_roleApp.Dispose();
            base.Dispose(disposing);
        }
    }
}