﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using SSO.Application.Interfaces;
using SSO.Application.ViewsModels;

namespace SSO.Presentation.MVC.Controllers
{
    [Authorize]
    public class SistemasController : Controller
    {
        private readonly ISistemaAppService _sistemaApp;

        public SistemasController(ISistemaAppService sistemaApp)
        {
            _sistemaApp = sistemaApp;
        }

        public ActionResult Index()
        {
            return View(_sistemaApp.GetAll());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var sistema = _sistemaApp.GetById(id.Value);
            if (sistema == null)
            {
                return HttpNotFound();
            }
            return View(sistema);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SistemaVm sistema)
        {
            if (!ModelState.IsValid) return View(sistema);
            _sistemaApp.Add(sistema);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            
            var sistema = _sistemaApp.GetById(id.Value);
            if (sistema == null)
            {
                return HttpNotFound();
            }
            return View(sistema);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SistemaVm sistema)
        {
            if (!ModelState.IsValid) return View(sistema);
            _sistemaApp.Update(sistema);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var sistema = _sistemaApp.GetById(id.Value);
            if (sistema == null) return HttpNotFound();

            return View(sistema);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _sistemaApp.Remove(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) _sistemaApp.Dispose();
            base.Dispose(disposing);
        }
    }
}