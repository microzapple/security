﻿using System.Web;
using System.Web.Mvc;
using IdentityServer3.Core.Extensions;
using SSO.Application.Interfaces;
using SSO.Application.ViewsModels;
using SSO.Presentation.MVC.IdentityServer;

namespace SSO.Presentation.MVC.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUsuarioAppService _usuarioApp;

        public AccountController(IUsuarioAppService usuarioApp)
        {
            _usuarioApp = usuarioApp;
        }

        public ActionResult Login(string id)
        {
            return View(new LoginVm { Identifier = id });
        }

        [HttpPost]
        public ActionResult Login(LoginVm model)
        {
            if (!ModelState.IsValid) return View(model);

            var user = _usuarioApp.Authenticate(model);

            //todo Informar "Usuário/Senha incorreta"
            if (user == null) return View(model);
            var env = Request.GetOwinContext().Environment;

            env.IssueLoginCookie(new IdentityServer3.Core.Models.AuthenticatedLogin
            {
                Subject = user.UsuarioId.ToString(),
                Name = user.FullName,
                Claims = Users.GetClaims(user)
            });
            var msg = env.GetSignInMessage(model.Identifier);
            var returnUrl = msg.ReturnUrl;
            env.RemovePartialLoginCookie();

            return Redirect(returnUrl);
        }
    }
}