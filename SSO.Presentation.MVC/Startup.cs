﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Web.Helpers;
using IdentityServer3.Core;
using IdentityServer3.Core.Configuration;
using IdentityServer3.Core.Services;
using Microsoft.IdentityModel.Protocols;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using Owin;
using SSO.Presentation.MVC.Helpers;
using SSO.Presentation.MVC.IdentityServer;
using AuthenticationOptions = IdentityServer3.Core.Configuration.AuthenticationOptions;

namespace SSO.Presentation.MVC
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            AntiForgeryConfig.UniqueClaimTypeIdentifier = Constants.ClaimTypes.Subject;
            JwtSecurityTokenHandler.InboundClaimTypeMap = new Dictionary<string, string>();

            app.Map("/identity", coreApp =>
            {
                var factory = new IdentityServerServiceFactory()
                    .UseInMemoryClients(Clients.Get())
                    .UseInMemoryScopes(Scopes.Get());
                
                factory.UserService = new Registration<IUserService, CustomLoginPageUserService>();

                var options = new IdentityServerOptions
                {
                    SiteName = "Security",
                    SigningCertificate = LoadCertificate(),
                    Factory = factory,
                    //RequireSsl = false,
                    //automatically redirect back to the client after logout
                    AuthenticationOptions = new AuthenticationOptions { EnablePostSignOutAutoRedirect = true }
                };
                coreApp.UseIdentityServer(options);
            });

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = "Cookies"
            });

            app.UseOpenIdConnectAuthentication(new OpenIdConnectAuthenticationOptions
            {
                Authority = "https://localhost:44305/identity",
                ClientId = "mvc",
                Scope = "openid profile roles",
                RedirectUri = "https://localhost:44305/",
                ResponseType = "id_token token",
                SignInAsAuthenticationType = "Cookies",
                UseTokenLifetime = false,
                Notifications = new OpenIdConnectAuthenticationNotifications
                {
                    SecurityTokenValidated = security =>
                    {
                        var id = security.AuthenticationTicket.Identity;

                        // we want to keep first name, last name, subject and roles
                        var givenName = id.FindFirst(Constants.ClaimTypes.GivenName);
                        var familyName = id.FindFirst(Constants.ClaimTypes.FamilyName);
                        var sub = id.FindFirst(Constants.ClaimTypes.Subject);
                        var claims = id.FindAll(Constants.ClaimTypes.Role);

                        // create new identity and set name and role claim type
                        var nid = new ClaimsIdentity(id.AuthenticationType, Constants.ClaimTypes.GivenName, Constants.ClaimTypes.Role);
                        
                        if (givenName != null) nid.AddClaim(givenName);
                        if (familyName != null) nid.AddClaim(familyName);
                        if (sub != null) nid.AddClaim(sub);
                        if (claims != null) nid.AddClaims(claims);

                        // add some other app specific claim
                        nid.AddClaim(new Claim("app_specific", "some data"));

                        // keep the id_token for logout - to avoid phishing
                        nid.AddClaim(new Claim("id_token", security.ProtocolMessage.IdToken));

                        // add access token for sample API
                        //nid.AddClaim(new Claim("access_token", security.ProtocolMessage.AccessToken));

                        // keep track of access token expiration
                        nid.AddClaim(new Claim("expires_at", DateTimeOffset.Now.AddMinutes(30).ToString()));

                        security.AuthenticationTicket = new AuthenticationTicket(nid, security.AuthenticationTicket.Properties);

                        return Task.FromResult(0);
                    },
                    RedirectToIdentityProvider = n =>
                    {
                        if (n.ProtocolMessage.RequestType == OpenIdConnectRequestType.LogoutRequest)
                        {
                            var idTokenHint = n.OwinContext.Authentication.User.FindFirst("id_token");
                            if (idTokenHint != null) n.ProtocolMessage.IdTokenHint = idTokenHint.Value;
                        }

                        return Task.FromResult(0);
                    }
                }
            });

            app.UseResourceAuthorization(new AuthAuthorize());
        }

        private static X509Certificate2 LoadCertificate()
        {
            var file = string.Format(@"{0}IdentityServer\Certificates\idsrv3test.pfx", AppDomain.CurrentDomain.BaseDirectory);
            return new X509Certificate2(file, "idsrv3test");
        }
    }
}